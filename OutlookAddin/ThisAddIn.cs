﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Diagnostics;
using System.Windows.Forms;
using System.Reflection;

namespace OutlookAddin
{
    public partial class ThisAddIn
    {

        private Outlook.Explorer test;
        private Outlook.MailItem mailItem;
        private Outlook.Inspectors _inspectors;
        private Dictionary<Guid, InspectorWrapper> _wrappedInspectors;

        private string _panelName = "SlideBar";
        private Panel task;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            test = Application.ActiveExplorer();
            test.SelectionChange += Test_SelectionChange;
            test.BeforeFolderSwitch += Test_BeforeFolderSwitch;

            this.Application.ItemLoad += Application_ItemLoad;
            _wrappedInspectors = new Dictionary<Guid, InspectorWrapper>();
            _inspectors = Globals.ThisAddIn.Application.Inspectors;
            _inspectors.NewInspector += new Outlook.InspectorsEvents_NewInspectorEventHandler(WrapInspector);
        }

        private void Test_BeforeFolderSwitch(object NewFolder, ref bool Cancel)
        {
            var customPanel = Globals.ThisAddIn.CustomTaskPanes.FirstOrDefault(x => x.Title == _panelName);
            if (customPanel != null) Globals.ThisAddIn.CustomTaskPanes.Remove(customPanel);
        }

        private void Test_SelectionChange()
        {
            foreach (object selection in this.Application.ActiveExplorer().Selection)
            {
                mailItem = selection as Outlook.MailItem;
                if (mailItem != null)
                {
                    StaticVariables.currentItem = mailItem;
                    var customPanel = Globals.ThisAddIn.CustomTaskPanes.FirstOrDefault(x => x.Title == _panelName);
                    if (customPanel == null)
                    {
                        task = new Panel();
                        StaticVariables.currentPanel = task;
                        var newCustomPanel = Globals.ThisAddIn.CustomTaskPanes.Add(task, _panelName);
                        newCustomPanel.Visible = true;
                        newCustomPanel.Width = 300;
                    }

                    Job job = new Job();
                    job.DoJob();
                }
            }

    }
        private void Application_ItemLoad(object Item)
        {
            mailItem = Item as Outlook.MailItem;
            if (mailItem != null)
            {

            }
        }

        void WrapInspector(Outlook.Inspector inspector)
        {
            InspectorWrapper wrapper = InspectorWrapper.GetWrapperFor(inspector);
            if (wrapper != null)
            {
                wrapper.Closed += new InspectorWrapperClosedEventHandler(wrapper_Closed);
                _wrappedInspectors[wrapper.Id] = wrapper;
            }
        }

        void wrapper_Closed(Guid id)
        {
            _wrappedInspectors.Remove(id);
        }

       

      
        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO generated coed
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
