﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddin
{
    public class Job
    {
        private string url = "http://dev.marketsgroup.org/experiments/sidebar_info";
        public async void DoJob()
        {
            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            string postData = "to=" + StaticVariables.currentItem.To + "&from=" + StaticVariables.currentItem.SenderEmailAddress + "&subject=" + StaticVariables.currentItem.Subject + "";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            WebResponse response = await request.GetResponseAsync();
            var x = ((HttpWebResponse)response).StatusCode;

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);
            string responseFromServer = reader.ReadToEnd();
            foreach (Control control in StaticVariables.currentPanel.Controls)
            {
                if (control is WebBrowser)
                {
                    ((WebBrowser)control).DocumentText = responseFromServer;
                }
            }
               
        }
    }
}
